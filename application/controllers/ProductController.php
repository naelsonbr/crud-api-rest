<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ProductController extends CI_Controller
{

    public function index()
    {

        $arrays = array(
            'content' => 'product_pdf_view',
            'countUsers' => $this->users_model->getUsersCount(),
            'id' => array(),
            'nome' => array(),
            'login' => array(),
            'email' => array(),
            'senha' => array(),
            'data_criacao' => array(),
            'temp_expired_senha' => array(),
            'cod_autorizacao' => array(),
            'status_usuario' => array(),
            'cod_pessoa' => array(),
            'identified' => array()

        );

        $i = 0;
        foreach ($this->users_model->getUsers() as $key) {

            $arrays['id'][$i] = $key['id'];
            $arrays['nome'][$i] = $key['nome'];
            $arrays['login'][$i] = $key['login'];
            $arrays['email'][$i] = $key['email'];
            $arrays['senha'][$i] = $key['senha'];
            $arrays['data_criacao'][$i] = $key['data_criacao'];
            $arrays['temp_expired_senha'][$i] = $key['temp_expired_senha'];
            $arrays['cod_autorizacao'][$i] = $key['cod_autorizacao'];
            $arrays['status_usuario'][$i] = $key['status_usuario'];
            $arrays['cod_pessoa'][$i] = $key['cod_pessoa'];
            $arrays['identified'][$i] = $key['identified'];
            $i++;
        }


        $this->load->view('template', $arrays);
    }


    public function checkHaveProductUsersName()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (!empty($_POST['name'])) {

                if (!empty($this->relational_model->filterProduct($_POST['name']))) {


                    foreach ($this->relational_model->filterProduct($_POST['name']) as $key)
                        echo json_encode(array("successful" => true, "name" => $key['nome'], "id" => $key['id_aparelho']));

                } else {
                    echo json_encode(array("t" => true));
                }

            } else {
                echo json_encode(array("empty" => true));
            }
        }
    }


    public function displayPdf()
    {

        if (isset($_POST['t_name'])) {

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->Cell(190, 10, utf8_decode('Filtro!'), 0, 1, "C");

            $pdf->Ln(15);

            $pdf->SetFont('Arial', '', 14);
            $pdf->Cell(47, 7, utf8_decode('Usuario'), 1, 0, "C");
            $pdf->Cell(47, 7, utf8_decode('Aparelho'), 1, 0, "C");
            $pdf->Cell(47, 7, utf8_decode('Descricao'), 1, 0, "C");
            $pdf->Cell(47, 7, utf8_decode('Codigo'), 1, 0, "C");

            $pdf->Ln(10);
            foreach($this->relational_model->getProductById($_POST['t_id_product']) as $key){
                $pdf->Cell(47, 7, utf8_decode($_POST['t_name']), 0, 0, "C");
                $pdf->Cell(47, 7, utf8_decode($key['id']), 0, 0, "C");
                $pdf->Cell(47, 7, utf8_decode($key['descricao_aparelho']), 0, 0, "C");
                $pdf->Cell(47, 7, utf8_decode($key['codigo_aparelho']), 0, 0, "C");
            }

            $pdf->Output();

        }else {

            show_404("Algo ocorreu");
        }
    }

}