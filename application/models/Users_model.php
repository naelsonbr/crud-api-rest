<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{


    public function getUsers()
    {

        $query = $this->db->get('usuarios');
        return $query->result_array();

    }


    public function getUsersCount()
    {

        $query = $this->db->get('usuarios');
        if ($query->num_rows() > 0)
            return $query->num_rows();
    }

    public function insertDataUser($name, $login, $email, $password)
    {
        $fusionHor = new DateTimeZone('America/Sao_Paulo');
        $data = new DateTime();
        $data->setTimezone($fusionHor);

        $data = array(
            'nome' => $name,
            'login' => $login,
            'email' => $email,
            'senha' => md5($password),
            'data_criacao' => $data->format('d-m-Y H:i:s'),
            'temp_expired_senha' => 0,
            'cod_autorizacao' => 0,
            'status_usuario' => 0,
            'cod_pessoa' => 0,
            'identified' => md5(rand(1, 99999))
        );

//        $this->db->insert('usuarios', $data);

        //Exception para garantir um retorno preciso
        try {
            $this->db->insert('usuarios', $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    public function insertProduct($description, $code)
    {



        //Exception para garantir um retorno preciso
        try {

            $data = array(
                'descricao_aparelho' => $description,
                'codigo_aparelho' => $code
            );

            $this->db->insert('aparelho', $data);

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    public function insertProfile($nameProfile)
    {

        $data = array(
            'nome_perfil' => $nameProfile
        );

        //Exception para garantir um retorno preciso
        try {
            $this->db->insert('perfil', $data);
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    //Aqui tem um bug, de maiusculo e minusculo, diferencia
    //1 or 0
    public function verifyEmail($email)
    {
        $query = $this->db->select("email")->where('email', $email)->get('usuarios');

        if ($query->num_rows() > 0)

            return 1;
        else return 0;
    }

    //Aqui tem um bug, de maiusculo e minusculo, diferencia
    //1 or 0
    public function verifyLogin($login)
    {
        $query = $this->db->select("login")->where('login', $login)->get('usuarios');

        if ($query->num_rows() > 0)

            return 1;
        else return 0;
    }


    //Aqui tem um bug, de maiusculo e minusculo, diferencia
    //1 or 0
    public function verifyProduct($code)
    {
        $query = $this->db->select("codigo_aparelho")->where('codigo_aparelho', $code)->get('aparelho');

        if ($query->num_rows() > 0)

            return 1;
        else return 0;
    }


    //Aqui tem um bug, de maiusculo e minusculo, diferencia
    //1 or 0
    public function verifyProfileName($profileName)
    {
        $query = $this->db->select("nome_perfil")->where('nome_perfil', $profileName)->get('perfil');

        if ($query->num_rows() > 0)

            return 1;
        else return 0;
    }


}