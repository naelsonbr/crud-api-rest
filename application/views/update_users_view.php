<div class="col-md-12">
    <h1 class="display-4 text-center">Update e Delete</h1>

<form onsubmit="return false" id="update">
    <table class="table text-center">
        <thead>
        <tr>

            <th scope="col">NOME</th>
            <th scope="col">LOGIN</th>
            <th scope="col">EMAIL</th>
            <th scope="col">PASSWORD</th>
            <th scope="col">DATA CRIACAO</th>
            <th scope="col">COD AUTORIZACAO</th>
            <th scope="col">STATUS PESSOA</th>
            <th scope="col">CODIGO PESSOA</th>
            <th scope="col">UPDATE</th>
            <th scope="col">DELETE</th>

        </tr>
        </thead>
        <tbody>
        <?php for ($i = 0; $i < 1 ; $i++) {
            if (!empty($nome[$i]) AND $identified[$i]) {?>
            <tr>

                <td>

                    <label for="name"></label>
                    <input type="text" name="t_name" class="form-control" id="name" aria-describedby="emailHelp" value="<?= $nome[$i]; ?>"

                </td>
                <td>

                    <label for="login"></label>
                    <input type="text" name="t_login" class="form-control" id="login" aria-describedby="emailHelp" value="<?= $login[$i]; ?>"

                </td>

                <td>

                    <label for="email"></label>
                    <input type="email" name="t_email" class="form-control" id="email" aria-describedby="emailHelp" value="<?= $email[$i]; ?>"

                </td>


                <td>

                    <label for="password"></label>
                    <input type="password" name="t_password" class="form-control" id="password" aria-describedby="emailHelp" value="<?= $senha[$i]; ?>"

                </td>

                <td>

                    <label for="data_create"></label>
                    <input type="text" name="t_data_create" class="form-control" id="data_create" aria-describedby="emailHelp" value="<?= $data_criacao[$i]; ?>"

                </td>

                <td>

                    <label for="cod_authorization"></label>
                    <input type="text" name="t_cod_authorization" class="form-control" id="cod_authorization" aria-describedby="emailHelp" value="<?= $cod_autorizacao[$i]; ?>"

                </td>

                <td>

                    <label for="status_people"></label>
                    <input type="text" name="t_status_people" class="form-control" id="status_people" aria-describedby="emailHelp" value="<?= $status_usuario[$i]; ?>"

                </td>

                <td>

                    <label for="cod_people"></label>
                    <input type="text" name="t_cod_people" class="form-control" id="cod_people" aria-describedby="emailHelp" value="<?= $cod_pessoa[$i]; ?>"

                </td>


               <input type="hidden" name="t_identified"  value="<?= $identified[$i]; ?>">
                <td><button type="submit" style="background-color: transparent; border: none; margin-top: 25px; color: #00CC00;" id="<?php echo "id-edit".$i; ?>" ><i class="fas fa-wrench"></i></button></td>
                <td><button class="delete" style="background-color: transparent; border: none; margin-top: 25px; color: red;" id="<?php echo "id-delete".$i; ?>" value="<?= $identified[$i]; ?>" href=""><i class="fas fa-wrench"></i></button></td>

            </tr>

        <?php } else {?>
        <div class="alert alert-danger text-center" role="alert">
            Vazio
        </div>
        <?php }}?>
        </tbody>
    </table>

</form>
</div>

<script type="application/javascript">
    $(function () {
        $('.delete').click(function (e) {

          e.preventDefault();

            $.ajax({
                url: '<?= base_url('CrudUsersController/delete') ?>',
                type: "POST",
                data: {sendIdentified: $(this).val()},
                dataType: "json",
                success: function (json) {

                    if (json.deleted === true){
                        alert("Uusario Deletado");
                        location.reload();
                    }

                    if (json.exception === true){alert("Uma Exceção occorreu");}
                },
                error: function (result) {console.log("Error Exception");}
            });

        });


        $('#update').submit(function (obj) {

            obj.preventDefault();

            console.log($(this).serialize());
            $.ajax({
                url: '<?= base_url('CrudUsersController/update') ?>',
                type: "POST",
                data: $(this).serialize(),
                dataType: "json",
                success: function (json) {

                    if (json.successful === true){
                        alert("Atualizado!");
                        location.reload();
                    }
                    if (json.exception === true){alert("Uma Exceção occorreu");}

                },
                error: function (result) {console.log("Error Exception");}
            });

        });


    });
</script>