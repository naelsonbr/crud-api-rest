<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- Tecnica Criada e utilizada By Naelson -->
<style>
    #profile-style {background-color: #28A745 !important; color: white; border-radius: 5px;}
</style>

<div class="col-md-12">

<h1 class="display-4">Perfil</h1>

<form onsubmit="return false" id="profile">

    <div class="form-group">
        <label for="name_profile">Nome do Perfil</label>
        <input type="text" name="t_profile_name" class="form-control" id="name_profile" aria-describedby="emailHelp">
    </div>

    <button type="submit" class="btn btn-dark">Cadastrar</button>
</form>


</div>


<script type="application/javascript">

    $(function () {

        $('#profile').submit(function (obj) {

            obj.preventDefault();

            console.log($(this).serialize());

            $.ajax({

                type: 'POST',
                url: "<?= base_url("InputController/createProfile"); ?>",
                data: $(this).serialize(),
                dataType: 'json',
                success: function (json) {

                    if (json.have === true)
                        alert("Ja Existe Codigo");

                    if (json.successful === true)
                    {
                        alert("Perfil cadastrado com sucesso!");
                        location.reload();
                    }


                },

                error:function () {console.log("ERROR EXCEPTION");}

            });
        });
    });

</script>