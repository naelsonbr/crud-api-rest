-- Table: usuarios

-- DROP TABLE usuarios;

CREATE TABLE usuarios
(
  id serial NOT NULL,
  nome character varying(60) NOT NULL,
  login character varying(12) NOT NULL,
  email character varying(60) NOT NULL,
  senha character varying(45) NOT NULL,
  --data_criacao timestamp with time zone NOT NULL,
  data_criacao character varying(25) NOT NULL,
  temp_expired_senha integer,
  cod_autorizacao character varying(1),
  status_usuario character varying(1),
  cod_pessoa integer,
  identified character varying(32),
  CONSTRAINT usuarios_pkey PRIMARY KEY (id)
)

CREATE TABLE perfil
(
  id serial primary key not null,
  nome_perfil varchar(60)
)

CREATE TABLE aparelho
(
   id serial primary key not null,
   descricao_aparelho varchar(15),
   codigo_aparelho varchar (100)
)

CREATE TABLE usuarios_aparelhos
(
  id_usuario integer NOT NULL,
  id_aparelho integer NOT NULL
)

CREATE TABLE usuarios_perfil
(
  id_usuarios integer NOT NULL,
  id_perfil integer NOT NULL
)