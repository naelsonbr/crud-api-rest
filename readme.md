
## Como usar


Download backup
https://gitlab.com/naelsongs/codeigniter/blob/master/database.backup

## Para connectar com DB POSTGRESS, em, "application/config/database.php" seguintes alteracoes

	'dsn'	=> 'pgsql:host=localhost;port=5432;dbname=codeigniter',
	'hostname' => 'localhost',
	'username' => 'postgres',
	'password' => 'admin',
	'database' => 'codeigniter',
	'dbdriver' => 'pdo',


## Dependencia FPDF


componser install
"setasign/fpdf": "1.8.1"

Auto Load feito em
"application/config/condig.php"
$config['composer_autoload'] = FCPATH . 'vendor/autoload.php';


## Pasta com arquivos manipulados codigo fonte desenvolvido se encontra em

"application/controller/"
"application/views/"
"application/views/" //Bootstrap Template.php

## End Points utilizando postman.


* Exibir todos usuarios
* GET
* url: http://localhost/codeigniter/index.php/CrudController/list



* Atualizar login Name filtrando por ID
* POST
* url: http://localhost/codeigniter/index.php/CrudController/update
* Parameter (id,NewLogin)


* Deletar usuario pelo ID
* POST
* url: http://localhost/codeigniter/index.php/CrudController/delete
* Parameter (id)





## Tela principal onde feito cadastro
* http://localhost/codeigniter/index.php/usuario

## Outra tela responsável pela update, adicionei um MD5 para indenficar a lista
* http://localhost/codeigniter/index.php/user/show?identified=cbf22ab286e2ad4900bdf5d6a2e47009

## Registra aparelhos
* $route['aparelho'] = "HomeController/registerProduct";

## Registar perfil
* $route['perfil'] = "HomeController/registerProfile";
## Roda onde deleta e atuliza usuario
* $route['user/show'] = 'CrudUsersController/show';
## Cria PDS EMITE

* $route['product'] = 'ProductController';
* $route['product/pdf'] = 'ProductController/displayPdf';
* $route['profile'] = 'ProfileController';
* $route['profile/pdf'] = 'ProfileController/displayPdf';

## Tabelas
* https://pastebin.com/KepbwZAR

## Fotos da tela
* https://imgur.com/a/ibqPSkD

